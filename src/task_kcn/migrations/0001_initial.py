# Generated by Django 3.2 on 2023-05-04 01:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FormData',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('market', models.CharField(max_length=100)),
                ('project', models.CharField(max_length=100)),
                ('siteID', models.CharField(max_length=100)),
                ('activity', models.CharField(max_length=100)),
                ('status', models.CharField(max_length=100)),
                ('priority', models.CharField(max_length=100)),
                ('customer', models.CharField(max_length=100)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('siteAddress', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('country', models.CharField(max_length=100)),
                ('zipCode', models.CharField(max_length=100)),
                ('coordinates', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('source', models.CharField(max_length=100)),
                ('target', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=100)),
                ('lag', models.IntegerField(blank=True, default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('site_name', models.CharField(blank=True, max_length=100)),
                ('market', models.CharField(blank=True, max_length=100)),
                ('address', models.CharField(blank=True, max_length=100)),
                ('customer', models.CharField(blank=True, max_length=100)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('text', models.CharField(blank=True, max_length=100)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('duration', models.IntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('actual_days_on_site', models.PositiveIntegerField(default=0)),
                ('status', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(editable=False, primary_key=True, serialize=False)),
                ('market', models.CharField(blank=True, max_length=100)),
                ('project_name', models.CharField(blank=True, max_length=100)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('site_name', models.ForeignKey(db_column='SiteID', on_delete=django.db.models.deletion.CASCADE, related_name='Projects', to='task_kcn.site')),
            ],
        ),
    ]
