from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from gantt.models import Task as ats

# from gantt_rest_python.task_kcn.models import Task as ats
# from
import pytz
import logging
logger = logging.getLogger(__name__)

class Command(BaseCommand):
    # help = 'actualdays'
     def handle(self, *args, **options):
        for data in ats.objects.filter(~Q(status='Completed')):
            now = datetime.now()
            local_tz = pytz.timezone('UTC')
            utc_time = now.replace().astimezone(local_tz)
            time_threshold = now - data.start_date
            actual_days = time_threshold.days + 1

            if actual_days > -1:
                data.actual_days_on_site = actual_days
                data.save()

# if actual_days > -1;
# 	data.progress = (actual_days / data.duration) * 100
# 	data.save()