from .models import Task
from .models import Link
from .models import Site
from .models import Project
from rest_framework import serializers


class TaskSerializer(serializers.ModelSerializer):
    start_date = serializers.DateTimeField(format='%Y-%m-%d ')
    end_date = serializers.DateTimeField(format='%Y-%m-%d ')

    class Meta:
        model = Task
        fields = ('id', 'text', 'start_date', 'end_date', 'duration',
                   'created_at', 'updated_at', 'actual_days_on_site', 'status')

class LinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Link
        fields = ('id', 'source', 'target', 'type', 'lag')

class Site(serializers.ModelSerializer):
    class Meta:
        model = Site
        fields = ('id', 'site_name', 'market', 'address', 'customer', 'created_at', 'updated_at')


class Project(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'site_name', 'market', 'project_name', 'created_at', 'updated_at')
