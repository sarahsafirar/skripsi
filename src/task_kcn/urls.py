from django.urls import re_path, path
from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [

    re_path(r'^$', views.index, name='index'),
    path('base/', views.base, name="base"),
    path('site/', views.site, name="site"),
    path('project/', views.project, name="project"),
    url('TaskView/', views.TaskView.as_view(), name="TaskView"),
    path('taskdetails/', views.task_details, name="taskdetails"),
    re_path(r'^data/TaskView/(?P<pk>[0-9]+)$', views.task_update),
    re_path(r'^data/TaskView', views.task_add),
    re_path(r'^data/link/(?P<pk>[0-9]+)$', views.link_update),
    re_path(r'^data/link', views.link_add),
    re_path(r'^data/(.*)$', views.data_list),
]
urlpatterns = format_suffix_patterns(urlpatterns)