// Data tables
$(document).ready(function () {
    $('#site-list').DataTable({
        // scrollY: 300,
        scrollCollapse: true,
        paging: true,
        info: true,
        dom: "<'row mb-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
        "<'row'<'col-sm-12 mb-4'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    });
});


// chart.js
var colors = ['#FFC107', '#0B5ED7', '#198754', '#6C757D', '#DC3545', '#6c757d'];

// Task summary Chart
var taskChart = document.getElementById("task-summary");
var taskData = {
    labels: ["Planning", "Pending", "On Progress", "Finished"],
    datasets: [{
        data: [5, 7, 3, 4],
        backgroundColor: [colors[3], colors[0], colors[1], colors[2]],
        borderWidth: 3,
        pointBackgroundColor: colors[0]
    }]
};
const taskConfig = {
    type: 'doughnut',
    data: taskData,
    options: {
        animation: {
            duration: 1000,
            easing: 'easeOutQuad',
        },
        responsive: true,
        Animation: true,
        // cutout: 50,
        // radius: 70,
        // borderRadius: 50,
        borderColor: '#E4E4E4',
        plugins: {
            legend:{
                display: false,
            },
        },
    },
};
const chart1 = new Chart(taskChart, taskConfig);