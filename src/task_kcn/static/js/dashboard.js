// chart.js
var colors = ['#FFC107', '#0B5ED7', '#198754', '#6C757D', '#DC3545', '#6c757d'];

// Project Chart
var projectChart = document.getElementById("project-chart");
var projectData = {
    labels: ["Planning", "Pending", "On Progress", "Finished"],
    datasets: [{
        data: [6, 4, 8, 6],
        backgroundColor: [colors[3], colors[0], colors[1], colors[2]],
        // borderColor: [colors[0], colors[1], colors[2]],
        borderWidth: 3,
        pointBackgroundColor: colors[0]
    }]
};
const projectConfig = {
    type: 'doughnut',
    data: projectData,
    options: {
        animation: {
            duration: 1000,
            easing: 'easeOutQuad',
        },
        responsive: true,
        Animation: true,
        cutout: 50,
        radius: 75,
        // borderRadius: 50,
        borderColor: '#E4E4E4',
        plugins: {
            legend:{
                display: false,
            },
        },
    },
};
const chart1 = new Chart(projectChart, projectConfig);


// Site Chart
var siteChart = document.getElementById("site-chart");
var siteData = {
    labels: ["Issued", "Walked", "IX Active", "Closed Out"],
    datasets: [{
        data: [15, 26, 23, 14],
        backgroundColor: [colors[3], colors[0], colors[2], colors[4]],
    }], 
};
const siteConfig ={
    type: 'bar',
    data: siteData,
    options: {
        responsive: true,
        indexAxis: 'y',
        plugins: {
            legend:{
                display: false,
            },
        },
    },
};
const chart2 = new Chart(siteChart, siteConfig);


// crew Chart
var crewChart = document.getElementById("crew-chart");
var crewData = {
    labels: ["Tiger Team", "Bear Team", "Panda Team", "Lion Team", "Cat team", "Elephant Team"],
    datasets: [{
        label: "Tiger Team",
        data: [10, 8, 11, 6, 5, 3],
        backgroundColor: colors[0],
    }], 
};
const crewConfig ={
    type: 'bar',
    data: crewData,
    options: {
        Animation: true,
        responsive: true,
        indexAxis: 'y',
        plugins: {
            title:{
                display: true,
                fullSize: false,
                text: "Pending Task",
            },
            legend:{
                display: false,
            },
        },
    },
};
const chart3 = new Chart(crewChart, crewConfig);


// Data tables
$(document).ready(function () {
    $('#expense-table').DataTable({
        scrollY: 190,
        scrollCollapse: true,
        paging: false,
        info: false,
        dom: "<'row'<f'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    });
});


$(document).ready(function () {
    $('#project-tracker').DataTable({
        scrollY: 190,
        scrollCollapse: true,
        paging: false,
        info: false,
        dom: "<'row'<f'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
    });
});