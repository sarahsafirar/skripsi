// Data tables
// $(document).ready(function() {
//     $('#task-table').DataTable({
//         scrollX: 300,
//         scrollY: 300,
//         scrollCollapse: true,
//         paging: true,
//         info: true,
//         dom: "<'row mb-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
//             "<'row'<'col-sm-12 mb-4'tr>>" +
//             "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
//     });
// });

// $(document).ready(function() {
//     $('#report-table').DataTable({
//         // scrollX: 400,
//         // scrollY: 600,
//         scrollCollapse: true,
//         paging: true,
//         info: true,
//         dom: "<'row mb-3'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" +
//             "<'row'<'col-sm-12 mb-4'tr>>" +
//             "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
//     });
// });

<
script src = "https://unpkg.com/frappe-gantt@^1.3.0/dist/frappe-gantt.min.js" > < /script> <
    script > { /* // Mengambil data Gantt chart dari Django */ }
const tasks = JSON.parse('{{ tasks|safe }}');
const dependencies = JSON.parse('{{ dependencies|safe }}');

{ /* // Membuat chart menggunakan data tersebut */ }
const gantt = new Gantt("#gantt-chart", tasks, dependencies);
gantt.refresh(tasks, dependencies); <
/script>