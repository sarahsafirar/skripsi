from django.shortcuts import render, redirect
from task_kcn.models import Task, Link, FormData
from django.views.generic import View
from task_kcn.serializers import TaskSerializer, LinkSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import JsonResponse

import logging
logger = logging.getLogger(__name__)


def index(request):

    return render(request, 'task_kcn/index.html')


@api_view(['GET'])
def data_list(request, offset):
    if request.method == 'GET':
        tasks = Task.objects.all()
        links = Link.objects.all()
        taskData = TaskSerializer(tasks, many=True)
        linkData = LinkSerializer(links, many=True)
        return Response({
            "tasks": taskData.data,
            "links": linkData.data
        })


@api_view(['POST'])
def task_add(request):
    if request.method == 'POST':
        print('tester')
        serializer = TaskSerializer(data=request.data)
        print(serializer)

        if serializer.is_valid():
            task = serializer.save()
            return JsonResponse({'action': 'inserted', 'tid': task.id})
        return JsonResponse({'action': 'error'})


@api_view(['PUT', 'DELETE'])
def task_update(request, pk):
    try:
        task = Task.objects.get(pk=pk)
    except Task.DoesNotExist:
        return JsonResponse({'action': 'error2'})

    if request.method == 'PUT':
        serializer = TaskSerializer(task, data=request.data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({'action': 'updated'})
        return JsonResponse({'action': 'error'})

    if request.method == 'DELETE':
        task.delete()
        return JsonResponse({'action': 'deleted'})


@api_view(['POST'])
def link_add(request):
    if request.method == 'POST':
        serializer = LinkSerializer(data=request.data)
        print(serializer)

        if serializer.is_valid():
            link = serializer.save()
            return JsonResponse({'action': 'inserted', 'tid': link.id})
        return JsonResponse({'action': 'error'})


@api_view(['PUT', 'DELETE'])
def link_update(request, pk):
    try:
        link = Link.objects.get(pk=pk)
    except Link.DoesNotExist:
        return JsonResponse({'action': 'error'})

    if request.method == 'PUT':
        serializer = LinkSerializer(link, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({'action': 'updated'})
        return JsonResponse({'action': 'error'})

    if request.method == 'DELETE':
        link.delete()
        return JsonResponse({'action': 'deleted'})


def base(request):
    return render(request, 'defaultkcn/base.html')


class TaskView(View):
    def get(self, request):
        tasks = Task.objects.all()
        context = {
            'tasks': tasks,
        }
        return render(request, 'task/task.html', context)

    def post(self, request):
        logger.debug(request)
        if request.POST.get('action') == 'save':
            id = request.POST['taskId']
            market = request.POST['market']
            project = request.POST['project']
            siteID = request.POST['siteId']
            activity = request.POST['description']
            status = request.POST['taskStatus']
            priority = request.POST['priority']
            customer = request.POST['customer']
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            siteAddress = request.POST['address']
            city = request.POST['city']
            state = request.POST['state']
            country = request.POST['country']
            zipCode = request.POST['zipCode']
            coordinates = request.POST['coordinate']
            form_data = FormData(taskId=id, market=market, project=project, siteId=siteID, description=activity,
                                 taskStatus=status,
                                 priority=priority, customer=customer, start_date=start_date, end_date=end_date,
                                 address=siteAddress,
                                 city=city, state=state, country=country, zipCode=zipCode, coordinate=coordinates)


            form_data.save()
            return redirect('success')

        else:
            form_data = FormData()

        return render(request, 'TaskView', {'form_data': form_data})


def task_details(request):
    return render(request, 'task/taskdetails.html')


def site(request):
    # sites = Site.objects.all()
    # context = {
    #     'sites': sites,
    # }
    return render(request, 'site/site.html')


def project(request):
    # projects = Project.objects.all()
    # context = {
    #     'projects': projects,
    # }
    return render(request, 'project/project.html')


def data_view(request):
    data = FormData.objects.all()
    return render(request, 'TaskView', {'data':data})

