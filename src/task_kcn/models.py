from django.db import models
# from management import
# Create your models here.


class Site(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    site_name = models.CharField(blank=True, max_length=100)
    market = models.CharField(blank=True, max_length=100)
    address = models.CharField(blank=True, max_length=100)
    customer = models.CharField(blank=True, max_length=100)
    created_at = models.DateTimeField(blank=False, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
#
#
class Project(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    site_name = models.ForeignKey(Site, related_name="Projects",db_column="SiteID", on_delete=models.CASCADE)
    market = models.CharField(blank=True, max_length=100)
    project_name = models.CharField(blank=True, max_length=100)
    created_at = models.DateTimeField(blank=False, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    #status

# buat table site project
# siteid, projectid, status, createad, updated

class Task(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    text = models.CharField(blank=True, max_length=100)
    # projectID = models.ForeignKey(Project, related_name="Task", db_column="ProjectID", on_delete=models.CASCADE)
    # market = models.CharField(blank=True, max_length=100)
    # siteID = models.IntegerField()
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    duration = models.IntegerField()
    # parent = models.CharField(max_length=100)
    # sort_order = models.IntegerField(default=0)
    created_at = models.DateTimeField(blank=False, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    actual_days_on_site = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=100, blank=False, null=True)
    # team = models.CharField(blank=True, max_length=100)
    # crew = models.CharField(blank=True, max_length=100)
    # priority = models.CharField(blank=True, max_length=100)

    # class Meta:
    #     db_table = 'kcn_task'


class Link(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    source = models.CharField(max_length=100)
    target = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    lag = models.IntegerField(blank=True, default=0)

# tabel team

class FormData(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    market = models.CharField(max_length=100)
    project = models.CharField(max_length=100)
    siteID = models.CharField(max_length=100)
    activity = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    priority = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    siteAddress = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    zipCode = models.CharField(max_length=100)
    coordinates = models.CharField(max_length=100)


